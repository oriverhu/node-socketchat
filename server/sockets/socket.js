const { io } = require('../server');

const { Usuarios } = require('./../classes/usuarios');
const usuarios = new Usuarios();

const { crearMensaje } = require('./../utils/utils')

io.on('connection', (client) => {


    client.on('entrarChat', (data, callback) => {
        if (!data.nombre || !data.sala) {
            return callback({
                error: true,
                mensaje: 'El nombre y sala son necesarios'
            })
        }

        client.join(data.sala)

        let personas = usuarios.agregarPersona(client.id, data.nombre, data.sala);

        callback(
            personas
        )

        client.broadcast.to(data.sala).emit('listaPersona', usuarios.getPeronaPorSala(data.sala))
        client.broadcast.to(data.sala).emit('crearMensaje', crearMensaje(data.nombre, 'se unió al chat'))

    })

    client.on('disconnect', () => {
        let personaBorrada = usuarios.borrarPersona(client.id);

        client.broadcast.to(personaBorrada.sala).emit('crearMensaje', crearMensaje(personaBorrada.nombre, 'abandonó el chat'))
        client.broadcast.to(personaBorrada.sala).emit('listaPersona', usuarios.getPeronaPorSala(personaBorrada.sala))
    });

    client.on('crearMensaje', (data, callback) => {
        let persona = usuarios.getPersona(client.id);

        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.to(data.sala).emit('crearMensaje', mensaje);

        callback(mensaje);
    });

    client.on('mensajePrivado', (data) => {
        let persona = usuarios.getPersona(client.id);
        client.broadcast.to(data.para).emit('mensajePrivado', crearMensaje(persona.nombre, data.mensaje))
    });

});